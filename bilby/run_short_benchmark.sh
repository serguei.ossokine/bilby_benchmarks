#!/bin/bash
export OMP_NUM_THREADS=1

sed -i "s/n-parallel=1/n-parallel=$1/g" bbh_short.ini
bilby_pipe bbh_short.ini
python ../sanitize_bilby_bash_file.py out_bbh_short/submit/bash_bbh_short.sh  short
bash out_bbh_short/submit/bash_bbh_short.sh
python ../extract_bilby_timing_results.py out_bbh_short/log_data_analysis/ err > bilby_result_short.dat
