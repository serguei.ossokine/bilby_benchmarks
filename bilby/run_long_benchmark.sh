#!/bin/bash
export OMP_NUM_THREADS=1

sed -i "s/n-parallel=1/n-parallel=$1/g" bbh_long.ini
bilby_pipe bbh_long.ini
python ../sanitize_bilby_bash_file.py out_bbh_long/submit/bash_bbh_long.sh long
bash out_bbh_long/submit/bash_bbh_long.sh 
python ../extract_bilby_timing_results.py out_bbh_long/log_data_analysis/ err > bilby_result_long.dat
