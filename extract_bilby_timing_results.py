#!/usr/bin/env python3

import glob
import re
import numpy as np
import sys

def get_work(dirname,suffix):
    """ Get the total work done, defined as the sum of 1/(sampling time) for all
    analyses."""
    files = glob.glob(f"{dirname}/*.{suffix}")
    work = 0.0
    for fname in files:
        with open(fname,"r") as fp:
            everything = "".join(fp.readlines())

        # Find the output in the logs
        pt = re.compile("Sampling time\s*[:|=]\s*(\d*):(\d*):(\d*\.?\d*)")
        #pt = re.compile("Sampling time\s*[:|=]s*")
        result = re.findall(pt,everything)[0]
        # Convert to float
        h,m,s = [float(x) for x in result]
        # Total number of seconds
        work+=1/(h*3600+m*60+s)
    return work

if __name__ == "__main__":
    dirname = sys.argv[1]
    suffix = sys.argv[2]
    work = get_work(dirname,suffix)
    print(work)
